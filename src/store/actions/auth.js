import * as actionTypes from './actionTypes';
import axios from "axios";


export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    }
}

export const authSuccess = (token, role) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token,
        role: role
    }
}

export const authFail = error => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    }
}

export const logout = () => {
    localStorage.removeItem('user');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('current_user_id');
    localStorage.removeItem('current_user_name');
    return {
        type: actionTypes.AUTH_LOGOUT
    };
}

export const checkAuthTimeout = expirationTime => {
    return dispatch => {
        setTimeout(()=>{
            dispatch(logout())
        },expirationTime*1000)
    };
}

export const authLogin = (email,password) => {
    return dispatch => {
        dispatch(authStart())
        axios.post('http://127.0.0.1:8000/api/login',{
            email: email,
            password: password
        })
            .then(res => {
                const token = res.data.token;
                const rolee = res.data.user.role
                console.log('tokenul php ',res.data.token);
                console.log('id user',res.data.user.id)
                console.log('name of user',res.data.user.name)
                localStorage.setItem('current_user_id',res.data.user.id)
                localStorage.setItem('current_user_name',res.data.user.name)
                const expirationDate = new Date(new Date().getTime() + 3600*1000);
                localStorage.setItem('token',token);
                localStorage.setItem('rolee',rolee);
                localStorage.setItem('expirationDate',expirationDate);
                dispatch(authSuccess(token,rolee));
                dispatch(checkAuthTimeout(3600));
            })
            .catch(err => {
                dispatch(authFail(err));
            })
    };
}

export const authSignup = (username, email, password1, password2,medical_services, role) => {
    return dispatch => {
        dispatch(authStart());
        axios.post('http://127.0.0.1:8000/api/register', {
            name: username,
            email: email,
            password: password1,
            password_confirmation: password2,
            medical_services: medical_services,
            role: role,
        })
        .then(res => {
            const token = res.data.token;
            const rolee = res.data.user.role
            const expirationDate = new Date(new Date().getTime() + 3600 * 1000);
            localStorage.setItem('current_user_id',res.data.user.id);
            localStorage.setItem('token', token);
            localStorage.setItem('rolee',rolee);
            localStorage.setItem('expirationDate', expirationDate);
            dispatch(authSuccess(token,rolee));
            dispatch(checkAuthTimeout(3600));
        })
        .catch(err => {
            dispatch(authFail(err))
        })
    }
}

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        const rolee = localStorage.getItem('rolee');
        if(token === undefined){
            dispatch(logout());
        }else{
            const expirationDate = new Date(localStorage.getItem('expirationDate'))
            if(expirationDate <= new Date()){
                dispatch(logout());
            }else{
                dispatch(authSuccess(token,rolee));
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));
            }
        }
    }
}