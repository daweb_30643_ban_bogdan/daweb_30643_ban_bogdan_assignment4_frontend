import React from 'react';
import './tarife-container.css';


class TarifeContainer extends React.Component {
    render(){
        let lang = localStorage.getItem('language')
        return(
            <>
              {lang === 'ro'?
            <div className={'tabel'}>
                <table id="customers">
                  <tr>
                    <th>Serviciu</th>
                    <th>Durata</th>
                    <th>Tarif</th>
                  </tr>
                  <tr>
                    <td>Consultatie</td>
                    <td>10min - 30 min</td>
                    <td>100 RON</td>
                  </tr>
                  <tr>
                    <td>Albire dinti</td>
                    <td>30 min</td>
                    <td>100 RON</td>
                  </tr>
                  <tr>
                    <td>Extractie dentara</td>
                    <td>1 h</td>
                    <td>100 RON</td>
                  </tr>
                  <tr>
                    <td>Plomba</td>
                    <td>1 h - 1h 30 min</td>
                    <td>200 RON</td>
                  </tr>
                  <tr>
                    <td>Implant dentar</td>
                    <td>1 h</td>
                    <td>1500 RON</td>
                  </tr>
                </table>
            </div>
            :
            <div className={'tabel'}>
                <table id="customers">
                  <tr>
                    <th>Service</th>
                    <th>Duration</th>
                    <th>Price</th>
                  </tr>
                  <tr>
                    <td>Consult</td>
                    <td>10min - 30 min</td>
                    <td>100 RON</td>
                  </tr>
                  <tr>
                    <td>Teeth whitening</td>
                    <td>30 min</td>
                    <td>100 RON</td>
                  </tr>
                  <tr>
                    <td>Tooth extraction</td>
                    <td>1 h</td>
                    <td>100 RON</td>
                  </tr>
                  <tr>
                    <td>Filling teeth</td>
                    <td>1 h - 1h 30 min</td>
                    <td>200 RON</td>
                  </tr>
                  <tr>
                    <td>Dental implant</td>
                    <td>1 h</td>
                    <td>1500 RON</td>
                  </tr>
                </table>
            </div>}
            </>
        );
    }
}
export default TarifeContainer;