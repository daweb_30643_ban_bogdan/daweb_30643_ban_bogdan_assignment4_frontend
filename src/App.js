import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import HomeContainer from "./home/home-container";
import NavigBar from "./navigBar";
import ContactContainer from "./contact/contact-container";
import DoctorsContainer from "./doctors/doctors-container";
import AboutContainer from "./about/about-container";
import TarifeContainer from "./tarife/tarife-container";
import NewsContainer from "./news/news-container";
import LoginContainer from "./auth/login-container";
import RegisterUserContainer from "./auth/register-user-container";
import * as actions from "./store/actions/auth";
import {connect} from "react-redux";
import DetailsContainer from "./auth/details-container";
import AppointmentContainer from "./appointment/appointment-container";
import DocsContainer from "./appointment/docs-container";
import GraphContainer from "./appointment/graph-container";


class App extends React.Component {

    componentDidMount() {
        this.props.onTryAutoSignup();
    }

    render() {
    return (
        <div>
            <Router>
                <div>
                    <NavigBar {...this.props}/>
                    <Switch>

                        <Route
                            exact
                            path='/graph'
                            component={GraphContainer}
                        />

                        <Route
                            exact
                            path='/docs'
                            component={DocsContainer}
                        />

                        <Route
                            exact
                            path='/makeapp'
                            component={AppointmentContainer}
                        />

                        <Route
                            exact
                            path='/details'
                            component={DetailsContainer}
                        />

                        <Route
                            exact
                            path='/login'
                            component={LoginContainer}
                        />

                        <Route
                            exact
                            path='/useradd'
                            component={RegisterUserContainer}
                        />

                        <Route
                            exact
                            path={'/'}
                            component={HomeContainer}
                        />
                        <Route
                            exact
                            path={'/news'}
                            component={NewsContainer}
                        />
                        <Route
                            exact
                            path={'/tarife'}
                            component={TarifeContainer}
                        />
                        <Route
                            exact
                            path={'/about'}
                            component={AboutContainer}
                        />
                        <Route
                            exact
                            path={'/medici'}
                            component={DoctorsContainer}
                        />
                        <Route
                            exact
                            path={'/contact'}
                            component={ContactContainer}
                        />
                    </Switch>
                </div>
            </Router>
        </div>
    );
  }
}

const mapStateeToProps = state => {
    return {
        isAuthenticated: state.token !== null,
        role: state.role
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onTryAutoSignup: () => dispatch(actions.authCheckState())
    }
}


export default connect(mapStateeToProps,mapDispatchToProps)(App);
