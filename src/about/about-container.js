import React from 'react';
import intro from './intro.mp4';
class AboutContainer extends React.Component {
    render(){
        return(
            <div>
                <video controls muted autoPlay >
                  <source src={intro} type="video/mp4"/>
                Your browser does not support the video tag.
                </video>
            </div>
        );
    }
}
export default AboutContainer;