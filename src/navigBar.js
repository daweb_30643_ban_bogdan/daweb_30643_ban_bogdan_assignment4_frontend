import React from 'react';
import './navigBar.css';
import {Nav, NavLink} from "reactstrap";
import {connect} from "react-redux";
import * as actions from './store/actions/auth';
import {withRouter} from "react-router-dom";


let lang  = localStorage.getItem('language')

const NavigBar = (props) => (
    <>
    {lang === 'ro'?
    <div className="topnav">
        <a className="active" href="/">Acasa</a>
        <a href="/news">Noutati</a>
        <a href="/about">Despre noi</a>
        <a href="/medici">Medici</a>
        <a href="/tarife">Servicii & tarife</a>
        <a href="/contact">Contact</a>
        {props.isAuthenticated ?
            props.role === 'normal'?
                <Nav className="ml-auto" navbar>
                    <NavLink align={'rightTop'} href={'/makeapp'}>Programeaza-te</NavLink>
                    <NavLink align={'rightTop'} href={'/details'}>Detalii</NavLink>
                    <NavLink align={'rightTop'} onClick={props.logout} href={'/'}>Delogare</NavLink>
                </Nav>
                :
                <Nav className="ml-auto" navbar>
                    <NavLink align={'rightTop'} href={'/graph'}>Grafic programari</NavLink>
                    <NavLink align={'rightTop'} href={'/docs'}>Vezi programari</NavLink>
                    <NavLink align={'rightTop'} href={'/details'}>Detalii</NavLink>
                    <NavLink align={'rightTop'} onClick={props.logout} href={'/'}>Delogare</NavLink>
                </Nav>
            :
            <Nav className="ml-auto" navbar>
                <NavLink align={'rightTop'} href="/login">Logare</NavLink>
                <NavLink align={'rightTop'} href="/useradd">Inregistrare</NavLink>
            </Nav>
        }
    </div>
    :
    <div className="topnav">
        <a className="active" href="/">Home</a>
        <a href="/news">News</a>
        <a href="/about">About us</a>
        <a href="/medici">Doctors</a>
        <a href="/tarife">Services & prices</a>
        <a href="/contact">Contact</a>
        {props.isAuthenticated ?
            props.role === 'normal'?
                <Nav className="ml-auto" navbar>
                    <NavLink align={'rightTop'} href={'/makeapp'}>Make appointment</NavLink>
                    <NavLink align={'rightTop'} href={'/details'}>Details</NavLink>
                    <NavLink align={'rightTop'} onClick={props.logout} href={'/'}>Logout</NavLink>
                </Nav>
                    :
                <Nav className="ml-auto" navbar>
                    <NavLink align={'rightTop'} href={'/graph'}>Apps graph</NavLink>
                    <NavLink align={'rightTop'} href={'/docs'}>See appointments</NavLink>
                    <NavLink align={'rightTop'} href={'/details'}>Details</NavLink>
                    <NavLink align={'rightTop'} onClick={props.logout} href={'/'}>Logout</NavLink>
                </Nav>

            :
            <Nav className="ml-auto" navbar>
                <NavLink align={'rightTop'} href="/login">Login</NavLink>
                <NavLink align={'rightTop'} href="/useradd">Register</NavLink>
            </Nav>
        }
    </div>}
    </>
)
const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(actions.logout())
    }
}

export default withRouter(connect(null,mapDispatchToProps)(NavigBar));
// export default NavigBar;