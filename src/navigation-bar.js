import React from 'react';

import logo from './logo.svg';

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

let lang = localStorage.getItem('language')

const NavigationBar = () => (
    <div>
        {lang === 'ro'?
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"} alt={'logos'}
                     height={"35"} />
            </NavbarBrand>
            <Nav className="mr-auto" navbar>

                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle style = {textStyle} nav caret>
                       Menu
                    </DropdownToggle>
                    <DropdownMenu right >

                        <DropdownItem>
                            <NavLink href="/patient">Patients</NavLink>
                        </DropdownItem>

                        <DropdownItem>
                            <NavLink href="/login">Login</NavLink>
                        </DropdownItem>

                        <DropdownItem>
                            <NavLink href="/doctor">Doctors</NavLink>
                        </DropdownItem>
                        <DropdownItem>
                            <NavLink href="/caregiver">Caregivers</NavLink>
                        </DropdownItem>

                    </DropdownMenu>
                </UncontrolledDropdown>

            </Nav>
        </Navbar>
            :
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"} alt={'logos'}
                     height={"35"} />
            </NavbarBrand>
            <Nav className="mr-auto" navbar>

                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle style = {textStyle} nav caret>
                       Menu
                    </DropdownToggle>
                    <DropdownMenu right >

                        <DropdownItem>
                            <NavLink href="/patient">Patients</NavLink>
                        </DropdownItem>

                        <DropdownItem>
                            <NavLink href="/login">Login</NavLink>
                        </DropdownItem>

                        <DropdownItem>
                            <NavLink href="/doctor">Doctors</NavLink>
                        </DropdownItem>
                        <DropdownItem>
                            <NavLink href="/caregiver">Caregivers</NavLink>
                        </DropdownItem>

                    </DropdownMenu>
                </UncontrolledDropdown>

            </Nav>
        </Navbar>}
    </div>
);

export default NavigationBar;
