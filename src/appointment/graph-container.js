import React from 'react';
import {connect} from "react-redux";
import axios from "axios";
import Chart from "./chart";
import moment from "moment/moment";

const DAYS = () => {
     const days = []
     const dateStart = moment()
     const dateEnd = moment().add(31, 'days')
     while (dateEnd.diff(dateStart, 'days') >= 0) {
      days.push(dateStart.format('D'))
      dateStart.add(1, 'days')
     }
     return days
   }

class GraphContainer extends React.Component {

    constructor() {
        super();
        this.state = {
            is_auth: null,
            roleee: '',
            chartData:{},
            tableData: []
        }
    }

    componentWillReceiveProps(newProps) {
        console.log(newProps.token)
        console.log(typeof newProps.role)
        this.setState({is_auth:newProps.token})
        this.setState({roleee:newProps.role})
    }

    componentDidMount() {
        let dname = localStorage.getItem('current_user_name');
        console.log("doc name = ",dname);
        this.fetchApps(dname)
        this.getChartData();
    }

    fetchApps(docName){
        axios.get('http://127.0.0.1:8000/api/apps/search/'+docName)
            .then(res => {
                console.log(res.data)
                this.setState({tableData: res.data});
            });
    }



    getChartData(){
        let arr = Array(31,[0,2,4,2,,0,0,1])
    // Ajax calls here
    this.setState({
      chartData:{
        labels: DAYS(),
        datasets:[
          {
            label:'nr of apps',
            data:arr
          }
        ]
      }
    });
  }

    render(){
        let tabData = this.state.tableData
        console.log(DAYS())
        return(
            <>
            {
                this.state.is_auth === null || this.state.roleee.localeCompare('doctor') !== 0 ? <h1>Permission denied</h1> :
                    <div>
                        Graph page
                        <br/>
                        {tabData
                                .map((item, i) => (
                                    <div className="slice" key={i}>
                                        Pacient name: {item.pacient_name}, <br/>
                                        Doctor name: {item.doctor_name}, <br/>
                                        Appointment date: {item.app_date}, <br/>
                                        <br/>
                                        <br/>
                                    </div>
                                ))}
                        <br/>
                        <Chart chartData={this.state.chartData} location="Massachusetts" legendPosition="bottom"/>
                    </div>
            }
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.token,
        role: state.role
    }

}
export default connect(mapStateToProps)(GraphContainer);