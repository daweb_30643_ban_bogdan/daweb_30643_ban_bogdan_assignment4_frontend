import React from 'react';
import axios from "axios";
import {connect} from "react-redux";
import Scheduler, {SchedulerData, ViewTypes, DATE_FORMAT} from 'react-big-scheduler';
import moment from 'moment';
import './calendar.css';


class DocsContainer extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            tableData: [],
            is_auth: null,
            roleee: '',


        }
    }



    componentWillReceiveProps(newProps) {
        console.log(newProps.token)
        console.log(typeof newProps.role)
        this.setState({is_auth:newProps.token})
        this.setState({roleee:newProps.role})
    }

    componentDidMount() {
        let dname = localStorage.getItem('current_user_name');
        console.log("doc name = ",dname);
        this.fetchApps(dname)
    }

    fetchApps(docName){
        axios.get('http://127.0.0.1:8000/api/apps/search/'+docName)
            .then(res => {
                console.log(res.data)
                this.setState({tableData: res.data});
            });
    }

    render(){
        let tabData = this.state.tableData
        return(
            <>
                {
                    this.state.is_auth === null || this.state.roleee.localeCompare('doctor') !== 0 ?
                        <h1>Permission denied</h1> :
                        <div>
                            {console.log(this.state.tableData)}
                            {tabData
                                .map((item, i) => (
                                    <div className="slice" key={i}>
                                        Pacient name: {item.pacient_name}, <br/>
                                        Doctor name: {item.doctor_name}, <br/>
                                        Appointment date: {item.app_date}, <br/>
                                        <br/>
                                        <br/>
                                    </div>
                                ))}

                            <table id="calendar">
                                <caption>May 2021</caption>
                                <tr className="weekdays">
                                    <th scope="col">Sunday</th>
                                    <th scope="col">Monday</th>
                                    <th scope="col">Tuesday</th>
                                    <th scope="col">Wednesday</th>
                                    <th scope="col">Thursday</th>
                                    <th scope="col">Friday</th>
                                    <th scope="col">Saturday</th>
                                </tr>

                                <tr className="days">
                                    <td className="day other-month">
                                        <div className="date">25</div>
                                    </td>
                                    <td className="day other-month">
                                        <div className="date">26</div>
                                    </td>
                                    <td className="day other-month">
                                        <div className="date">27</div>
                                    </td>
                                    <td className="day other-month">
                                        <div className="date">28</div>
                                    </td>
                                    <td className="day other-month">
                                        <div className="date">29</div>
                                    </td>


                                    <td className="day other-month">
                                        <div className="date">30</div>
                                    </td>
                                    <td className="day">
                                        <div className="date">1</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("01") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>

                                    <td className="day">
                                        <div className="date">2</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("02") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">3</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("03") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">4</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("04") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">5</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("05") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">6</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("06") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">7</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("07") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">8</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("08") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">9</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("09") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>

                                    <td className="day">
                                        <div className="date">10</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("10") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">11</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("11") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">12</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("12") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">13</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("13") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">14</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("14") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">15</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("15") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">16</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("16") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>


                                    <td className="day">
                                        <div className="date">17</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("17") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">18</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("18") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">19</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("19") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">20</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("20") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">21</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("21") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">22</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("22") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">23</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("23") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>

                                    <td className="day">
                                        <div className="date">24</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("24") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">25</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("25") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">26</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("26") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">27</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("27") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">28</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("28") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">29</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("29") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">30</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("30") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day">
                                        <div className="date">31</div>
                                        {tabData.map((item, i) => (
                                            item.app_date.slice(-2).localeCompare("31") === 0 ?
                                            <div className="slice" key={i}>
                                                Pacient name: {item.pacient_name}
                                            </div> :
                                                    <></>
                                        ))}
                                    </td>
                                    <td className="day other-month">
                                        <div className="date">1</div>
                                    </td>
                                    <td className="day other-month">
                                        <div className="date">2</div>
                                    </td>
                                    <td className="day other-month">
                                        <div className="date">3</div>
                                    </td>
                                    <td className="day other-month">
                                        <div className="date">4</div>
                                    </td>
                                    <td className="day other-month">
                                        <div className="date">5</div>
                                    </td>
                                    <td className="day other-month">
                                        <div className="date">6</div>
                                    </td>
                                </tr>

                            </table>

                        </div>
                }
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.token,
        role: state.role
    }

}
export default connect(mapStateToProps)(DocsContainer);