import React from 'react';
import {Button, Col, FormGroup, Input, Label, Row} from "reactstrap";
import validate from "./validators/mail-validators";


class MailForm extends React.Component{

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                email: {
                    value: '',
                    placeholder: 'Email...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        emailValidator: true
                    }
                },
                title: {
                    value: '',
                    placeholder: 'Title',
                    valid: false,
                    touched: false,
                },
                content: {
                    value: '',
                    placeholder: 'Message...',
                    valid: false,
                    touched: false,
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    // sendMail(mail){
    //  axios
    //  reloadHandler
    // }

    handleSubmit() {
        let mail = {
            email: this.state.formControls.email.value,
            title: this.state.formControls.title.value,
            content: this.state.formControls.content.value
        };

        console.log(mail);
        this.reloadHandler();
        //this.sendMail(mail)
    }

    render() {
        return (
            <div>
                <FormGroup id='email'>
                    <Label for='emailField'> Email: </Label>
                    <Input name='email' id='emailField' placeholder={this.state.formControls.email.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.email.value}
                           touched={this.state.formControls.email.touched? 1 : 0}
                           valid={this.state.formControls.email.valid}
                           required
                    />
                    {this.state.formControls.email.touched && !this.state.formControls.email.valid &&
                    <div className={"error-message"}> * Email must have a valid format</div>}
                </FormGroup>

                <FormGroup id='title'>
                    <Label for='titleField'> Address: </Label>
                    <Input name='title' id='titleField' placeholder={this.state.formControls.title.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.title.value}
                           touched={this.state.formControls.title.touched? 1 : 0}
                           valid={this.state.formControls.title.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='content'>
                    <Label for='contentField'> Age: </Label>
                    <Input name='content' id='contentField' placeholder={this.state.formControls.content.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.content.value}
                           touched={this.state.formControls.content.touched? 1 : 0}
                           valid={this.state.formControls.content.valid}
                           required
                    />
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

            </div>
        ) ;
    }

}

export default MailForm;