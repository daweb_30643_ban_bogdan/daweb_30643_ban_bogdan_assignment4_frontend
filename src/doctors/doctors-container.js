import React from 'react';
import DoctorPage from "./doctor-page";
import profile1 from '../profile1.jpg';
import profile2 from '../profile2.jpg';
import profile22 from '../profile3.jpeg';
import DoctorPage2 from "./doctor-page2";
class DoctorsContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            tableData:[]
        }
        this.dummyData = [
            {
                'poza': profile1,
                'nume': 'Popescu',
                'prenume':'Ion',
                'specializare':'Chirurg'
                },
                {
                    'poza':profile2,
                    'nume': 'Tiplea',
                    'prenume':'Vasile',
                    'specializare':'Tehnician dentar'
                },
                {
                    'poza':profile22,
                    'nume': 'Slavescu',
                    'prenume':'Mircea',
                    'specializare':'Dentist'
                }
        ]
    }

    render(){
        let lang = localStorage.getItem('language')
        return(
            <>
            {lang === 'en'?
            <div>
                <h1>Staff: </h1>
                <br/>
                <DoctorPage2 data = {this.dummyData}/>
            </div>
                :
            <div>

                <h1>Staf: </h1>
                <br/>
                <DoctorPage data = {this.dummyData}/>
            </div>}
            </>
        );
    }
}

export default DoctorsContainer;